/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Hidden attribute.
    /// </summary>
    public class HiddenAttribute: Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.HiddenAttribute"/> class.
        /// </summary>
        public HiddenAttribute ():
            this(true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.HiddenAttribute"/> class.
        /// </summary>
        /// <param name="hidden">If set to <c>true</c> hidden.</param>
        public HiddenAttribute(bool hidden)
        {
            Hidden = hidden;
        }

        /// <summary>
        /// Gets a value indicating whether the object having this attribute is hidden.
        /// </summary>
        /// <value><c>true</c> if hidden; otherwise, <c>false</c>.</value>
        public bool Hidden {
            get;
            private set;
        }
    }
}

