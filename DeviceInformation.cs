/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Logging;
using ProbeNet.Messages.Raw;
using System;
#if MICRO_FRAMEWORK
using System.Collections;
using System.Reflection;
#else
using System.Collections.Generic;
using Internationalization;
#endif

namespace ProbeNet.Backend
{
    /// <summary>
    /// This class provides information about a device.
    /// </summary>
    public class DeviceInformation
    {
#if MICRO_FRAMEWORK
        private IList installedLanguages;
        private string name;
#else
        private I18n i18n;
        private IList<string> installedLanguages;
        private TranslationString name;
#endif
        private string identifier;
        private Type classType;
        private byte[] icon;
        private int defaultBaudRate;
        private ManufacturerInformation manufacturer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.DeviceInformation"/> class.
        /// </summary>
        /// <param name="identifier">Identifier.</param>
        /// <param name="classType">Class type.</param>
        /// <param name="name">Name.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="defaultBaudRate">Default baud rate.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="installedLanguages">Installed languages.</param>
        /// <param name="manufacturer">Manufacturer.</param>
        /// <param name="hidden">If set to <c>true</c> hidden.</param>
        public DeviceInformation(
            string identifier,
            Type classType,
#if MICRO_FRAMEWORK
 string name,
#else
            TranslationString name,
#endif
 byte[] icon,
            int defaultBaudRate,
#if MICRO_FRAMEWORK
 IList installedLanguages,
#else
            I18n i18n,
            IList<string> installedLanguages,
#endif
 ManufacturerInformation manufacturer,
            bool hidden)
        {
            this.identifier = identifier;
            this.classType = classType;
            this.name = name;
            this.icon = icon;
            this.defaultBaudRate = defaultBaudRate;
#if !MICRO_FRAMEWORK
            this.i18n = i18n;
#endif
            this.installedLanguages = installedLanguages;
            this.manufacturer = manufacturer;
            Hidden = hidden;
        }

        /// <summary>
        /// Gets the type of the class.
        /// </summary>
        /// <value>The type of the class.</value>
        public Type ClassType
        {
            get
            {
                return classType;
            }
        }

        /// <summary>
        /// Gets the icon.
        /// </summary>
        /// <value>The icon.</value>
        public byte[] Icon
        {
            get
            {
                return icon;
            }
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public string Identifier
        {
            get
            {
                return identifier;
            }
        }

        /// <summary>
        /// Gets the translatable name.
        /// </summary>
        /// <value>The name.</value>
#if MICRO_FRAMEWORK
        public string Name
#else
        public TranslationString Name
#endif
        {
            get
            {
                return name;
            }
        }

        /// <summary>
        /// Gets the default baud rate. May be used to preselect the most common setting or factory default)
        /// when configuring a connection.
        /// </summary>
        /// <value>The default baud rate.</value>
        public int DefaultBaudRate
        {
            get
            {
                return defaultBaudRate;
            }
        }

        /// <summary>
        /// Gets the manufacturer.
        /// </summary>
        /// <value>The manufacturer.</value>
        public ManufacturerInformation Manufacturer
        {
            get
            {
                return manufacturer;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the device is hidden.
        /// </summary>
        /// <value><c>true</c> if hidden; otherwise, <c>false</c>.</value>
        public bool Hidden
        {
            get;
            private set;
        }

        /// <summary>
        /// Builds the handle.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="serialNumber">Serial number.</param>
        /// <param name="deviceDescriptions">Device descriptions.</param>
#if MICRO_FRAMEWORK
        public Handle BuildHandle(string serialNumber, IDictionary deviceDescriptions)
        {
            try {
                Type[] types = new Type[] { typeof(IList), typeof(string), typeof(IDictionary) };
                ConstructorInfo constructore = ClassType.GetConstructor(types);
                return constructore.Invoke(new object[] { installedLanguages, serialNumber, deviceDescriptions }) as Handle;
            } catch (Exception e) {
                Logger.Log(e);
                return null;
            }
        }
#else
        public Handle BuildHandle(string serialNumber, IDictionary<string, DeviceDescription> deviceDescriptions)
        {
            try {
                return Activator.CreateInstance(ClassType, i18n, installedLanguages, serialNumber, deviceDescriptions) as Handle;
            } catch (Exception e) {
                Logger.Log(e, "DeviceInformation cannot build handle for '{0}'", ClassType.FullName);
                return null;
            }
        }
#endif
    }
}

