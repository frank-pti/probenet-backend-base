/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using System.Reflection;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Resource icon attribute.
    /// </summary>
    public class ResourceIconAttribute : Attribute
    {
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.ResourceIconAttribute"/> class.
        /// </summary>
        /// <param name="resourcePrefix">Resource prefix.</param>
        /// <param name="name">Name.</param>
        public ResourceIconAttribute(string resourcePrefix, string name)
        {
            ResourcePrefix = resourcePrefix;
            this.name = name;
        }

        /// <summary>
        /// Gets or sets the resource prefix.
        /// </summary>
        /// <value>The resource prefix.</value>
        public string ResourcePrefix
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                return name;
            }
        }

        /// <summary>
        /// Gets the name of the resource.
        /// </summary>
        /// <value>The name of the resource.</value>
        public string ResourceName
        {
            get
            {
#if MICRO_FRAMEWORK
                return ResourcePrefix + "." + Name;
#else
				return String.Format("{0}.{1}", ResourcePrefix, Name);
#endif
            }
        }

        /// <summary>
        /// Gets the icon from assembly.
        /// </summary>
        /// <returns>The icon from assembly.</returns>
        /// <param name="assembly">Assembly.</param>
        public byte[] GetIconFromAssembly(Assembly assembly)
        {
            Stream readStream = null;
            try {
#if MICRO_FRAMEWORK
                return null;
#else
                 readStream = assembly.GetManifestResourceStream(ResourceName);
                 byte[] data = new byte[readStream.Length];
                 readStream.Read(data, 0, (int)readStream.Length);
                 return data;
#endif
            } catch (Exception exception) {
                throw exception;
            } finally {
                if (readStream != null) {
                    readStream.Close();
                }
            }
        }

        /// <summary>
        /// Gets the icon.
        /// </summary>
        /// <value>The icon.</value>
        public byte[] Icon
        {
            get
            {
#if MICRO_FRAMEWORK
                return null;
#else
                return GetIconFromAssembly(Assembly.GetCallingAssembly());
#endif
            }
        }
    }
}

