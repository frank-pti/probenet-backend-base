/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Internationalization;
using System.Collections.Generic;
#endif
using ProbeNet.Messages.Raw;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Base class for handles that load the example data from resources.
    /// </summary>
    public abstract class HandleWithExampleDataFromResources : Handle
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.HandleWithExampleDataFromResources"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="installedLanguages">Installed languages.</param>
        /// <param name="serialNumber">Serial number.</param>
        /// <param name="deviceDescriptions">Device descriptions.</param>
#if MICRO_FRAMEWORK
        protected HandleWithExampleDataFromResources(
            IList installedLanguages, string serialNumber, IDictionary deviceDescriptions) :
            base(installedLanguages, serialNumber, deviceDescriptions)
        {
        }
#else
        protected HandleWithExampleDataFromResources (
            I18n i18n, IList<String> installedLanguages, string serialNumber, IDictionary<string, DeviceDescription> deviceDescriptions):
            base(i18n, installedLanguages, serialNumber, deviceDescriptions)
        {
        }
#endif

        private ExampleDataResourceAttribute ExampleDataResource
        {
            get
            {
#if MICRO_FRAMEWORK
                object[] attributes = new object[0];
#else
                object[] attributes = GetType().GetCustomAttributes(typeof(ExampleDataResourceAttribute), false);
#endif
                if (attributes.Length == 0) {
                    return new ExampleDataResourceAttribute("", "");
                }

                ExampleDataResourceAttribute attribute = (attributes[0]) as ExampleDataResourceAttribute;
                return attribute;
            }
        }

        /// <inheritdoc/>
        public sealed override int ExampleDataCount
        {
            get
            {
                return ExampleDataResource.Count;
            }
        }

        /// <inheritdoc/>
        public sealed override byte[] GetExampleData(int index)
        {
            return ExampleDataResource.GetResource(GetType().Assembly, index);
        }
    }
}

