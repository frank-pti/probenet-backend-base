using Logging;
/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using System.Reflection;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Attribute for example data resources.
    /// </summary>
    public class ExampleDataResourceAttribute : Attribute
    {
        private string prefix;
        private string postfix;
        private string[] resources;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.ExampleDataResourceAttribute"/> class.
        /// </summary>
        /// <param name="prefix">Prefix.</param>
        /// <param name="postfix">Postfix.</param>
        /// <param name="resources">Resources.</param>
        public ExampleDataResourceAttribute(string prefix, string postfix, params string[] resources)
        {
            this.prefix = prefix;
            this.postfix = postfix;
            this.resources = resources;
        }

        /// <summary>
        /// Gets the amount of the resources.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get
            {
                return resources.Length;
            }
        }

        /// <summary>
        /// Gets the resource itself.
        /// </summary>
        /// <returns>The resource.</returns>
        /// <param name="assembly">Assembly.</param>
        /// <param name="index">Index.</param>
        public byte[] GetResource(Assembly assembly, int index)
        {
#if MICRO_FRAMEWORK
            string resourceName = prefix + resources[index] + postfix;
#else
            string resourceName = String.Format("{0}{1}{2}", prefix, resources[index], postfix);
#endif

            Stream readStream = null;
            byte[] data = null;
            try {
#if MICRO_FRAMEWORK
                Logger.Warning("Loading from resources is not supported by .NET MF");
#else
                System.Collections.Generic.List<String> allResourceNames =
                    new System.Collections.Generic.List<String>(assembly.GetManifestResourceNames());
                if (!allResourceNames.Contains(resourceName)) {
                    ModifyForVisualStudioCompiled(ref resourceName);
                }
                readStream = assembly.GetManifestResourceStream(resourceName);
                if (readStream != null) {
                    data = new byte[readStream.Length];
                    readStream.Read(data, 0, (int)readStream.Length);
                } else {
                    Logger.Warning("Cannot get read stream for resource '{0}'", resourceName);
                }
#endif
            } catch (Exception exception) {
                throw exception;
            } finally {
                if (readStream != null) {
                    readStream.Close();
                }
            }
            return data;
        }

#if !MICRO_FRAMEWORK
        private void ModifyForVisualStudioCompiled(ref string resourceName)
        {
            int lastButOnePointIndex = resourceName.LastIndexOf(".", resourceName.LastIndexOf(".") - 1);
            string part = resourceName.Substring(0, lastButOnePointIndex);
            string newpart = part.Replace("-", "_");
            resourceName = resourceName.Replace(part, newpart);
        }
#endif
    }
}
