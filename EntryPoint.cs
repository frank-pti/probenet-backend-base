/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Logging;
using System;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Internationalization;
#endif
using System.Reflection;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Base class for the backend entry points.
    /// </summary>
    public abstract class EntryPoint
    {
        private const string DefaultLanguage = "en";
        private const string LanguageResourceExtension = ".json";

#if !MICRO_FRAMEWORK
        private I18n i18n;
        private string i18nBasePath;
        private string i18nDomain;
#endif
        private Assembly assembly;
#if MICRO_FRAMEWORK
        private IList installedLanguages;
#else
        private List<String> installedLanguages;
#endif

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.EntryPoint"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="i18nBasePath">I18n base path.</param>
        /// <param name="i18nDomain">I18n domain.</param>
#if MICRO_FRAMEWORK
        protected EntryPoint()
#else
        protected EntryPoint(I18n i18n, string i18nBasePath, string i18nDomain)
#endif
        {
            assembly = GetType().Assembly;
#if !MICRO_FRAMEWORK
            this.i18n = i18n;
            this.i18nBasePath = i18nBasePath;
            this.i18nDomain = i18nDomain;
#endif
            this.installedLanguages = LoadInstalledLanguages();

#if !MICRO_FRAMEWORK
            HandleI18nLanguageChanged(i18n, i18n.Language);
            i18n.LanguageChanged += HandleI18nLanguageChanged;
        }

        private void HandleI18nLanguageChanged(I18n source, string language)
        {
            try {
                i18n.LoadFromResource(assembly, i18nDomain, BuildResourceName(language));
            } catch (Exception) {
                try {
                    i18n.LoadFromResource(assembly, i18nDomain, BuildResourceName(DefaultLanguage));
                } catch (Exception) {
                    // fail silently, return untranslated strings
                }
            }
        }

        private string BuildResourceName(string language)
        {
            return String.Format("{0}.{1}{2}", i18nBasePath, language, LanguageResourceExtension);
#endif
        }

#if MICRO_FRAMEWORK
        private IList LoadInstalledLanguages()
#else
        private List<string> LoadInstalledLanguages()
#endif
        {
#if MICRO_FRAMEWORK
            IList entries = new ArrayList() { "en" };
#else
            string[] names = assembly.GetManifestResourceNames();
            List<string> entries = new List<string>();

            foreach (string name in names) {
                if (name.StartsWith(i18nBasePath) &&
                    name.EndsWith(LanguageResourceExtension) &&
                    name.Length == BuildResourceName(DefaultLanguage).Length) {
                    entries.Add(name.Substring(i18nBasePath.Length + 1, DefaultLanguage.Length));
                }
            }
#endif
            return entries;
        }

        /// <summary>
        /// Loads the device information.
        /// </summary>
        /// <returns>The device information.</returns>
#if MICRO_FRAMEWORK
        public IList LoadDeviceInformation()
#else
        public IList<DeviceInformation> LoadDeviceInformation()
#endif
        {
#if MICRO_FRAMEWORK
            IList backends = new ArrayList();
            Type[] allTypes = assembly.GetTypes();
#else
            List<DeviceInformation> backends = new List<DeviceInformation>();
            Type[] allTypes = assembly.GetExportedTypes();
#endif
            foreach (Type type in allTypes) {
                DeviceInformation deviceInformation = GetDeviceInformation(type);
                if (deviceInformation != null) {
                    backends.Add(deviceInformation);
                }
            }
            return backends;
        }

        private DeviceInformation GetDeviceInformation(Type type)
        {
            if (!type.IsSubclassOf(typeof(Handle))) {
                return null;
            }
            if (type.IsAbstract) {
                return null;
            }

#if MICRO_FRAMEWORK
            SourceTypeIdentifierAttribute identifierAttribute = LoadAttribute(type) as SourceTypeIdentifierAttribute;
#else
            SourceTypeIdentifierAttribute identifierAttribute = LoadAttribute<SourceTypeIdentifierAttribute>(type);
#endif
            if (identifierAttribute == null) {
                LogMissingAttribute(type.FullName, typeof(SourceTypeIdentifierAttribute).Name);
            }

#if MICRO_FRAMEWORK            
#else
            TranslatableCaptionAttribute captionAttribute = LoadAttribute<TranslatableCaptionAttribute>(type);
            if (captionAttribute == null) {
                LogMissingAttribute(type.FullName, typeof(TranslatableCaptionAttribute).Name);
            }
#endif

#if MICRO_FRAMEWORK
            HiddenAttribute hiddenAttribute = LoadAttribute(type) as HiddenAttribute;
#else
            HiddenAttribute hiddenAttribute = LoadAttribute<HiddenAttribute>(type);
#endif
            if (hiddenAttribute == null) {
                hiddenAttribute = new HiddenAttribute(false);
            }

            byte[] icon = null;
            ManufacturerInformation manufacturer = null;
            int defaultBaudRate = 57600;
            if (!hiddenAttribute.Hidden) {
#if MICRO_FRAMEWORK
                ResourceIconAttribute iconAttribute = LoadAttribute(type) as ResourceIconAttribute;
#else
                ResourceIconAttribute iconAttribute = LoadAttribute<ResourceIconAttribute>(type);
#endif
                if (iconAttribute == null) {
                    LogMissingAttribute(type.FullName, typeof(ResourceIconAttribute).Name);
                    return null;
                }
                try {
                    icon = iconAttribute.GetIconFromAssembly(type.Assembly);
                } catch (Exception e) {
#if MICRO_FRAMEWORK
                    Logger.Log(e, "Error while loading icon for type '" + type.FullName + "' from resource path '" +
                        iconAttribute.ResourceName + "'");
#else
                    Logger.Log(e, 
                        "Error while loading icon for type {0} from resource path {1}.",
                        type.FullName,
                        iconAttribute.ResourceName);
#endif
                    return null;
                }

#if MICRO_FRAMEWORK
                ManufacturerAttribute manufacturerAttribute = LoadAttribute(type) as ManufacturerAttribute;
#else
                ManufacturerAttribute manufacturerAttribute = LoadAttribute<ManufacturerAttribute>(type);
#endif
                if (manufacturerAttribute == null) {
                    LogMissingAttribute(type.FullName, typeof(ManufacturerAttribute).Name);
                } else {
                    manufacturer = manufacturerAttribute.Manufacturer;
                }

#if MICRO_FRAMEWORK
                SerialPortDefaultSettingsAttribute serialAttribute = LoadAttribute(type) as SerialPortDefaultSettingsAttribute;
#else
                SerialPortDefaultSettingsAttribute serialAttribute = LoadAttribute<SerialPortDefaultSettingsAttribute>(type);
#endif
                if (serialAttribute == null) {
                    LogMissingAttribute(type.FullName, typeof(SerialPortDefaultSettingsAttribute).Name);
                } else {
                    defaultBaudRate = serialAttribute.BaudRate;
                }
            }

            return new DeviceInformation(
                identifierAttribute.Identifier,
                type,
#if MICRO_FRAMEWORK
 type.Name,
#else
                captionAttribute.GetTranslation(i18n),
#endif
 icon,
                defaultBaudRate,
#if !MICRO_FRAMEWORK
                i18n,
#endif
 installedLanguages,
                manufacturer,
                hiddenAttribute.Hidden);
        }

#if MICRO_FRAMEWORK
        private object LoadAttribute(Type type)
        {
            object[] attributes = new object[0];
#else
        private T LoadAttribute<T>(Type type)
            where T : class
        {
            object[] attributes = type.GetCustomAttributes(typeof(T), false);
#endif
            if (attributes.Length == 0) {
                return null;
            }
#if MICRO_FRAMEWORK
            return attributes[0];
#else
            return attributes[0] as T;
#endif
        }

        private void LogMissingAttribute(string typeName, string attributeName)
        {
#if MICRO_FRAMEWORK
            Logger.Warning("Type " + typeName + " is subclass of Handle, but is missing a " + attributeName +
                " attribute, or the the attribute cannot be loaeded.");
#else
            Logger.Warning("Type {0} is subclass of Handle, but is missing a {1} attribute", typeName, attributeName);
#endif
        }
    }
}

