/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if !MICRO_FRAMEWORK
using Internationalization;
#endif
using ProbeNet.Messages.Base;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Base class of all parsers.
    /// </summary>
    public abstract class Parser : IParser
    {
        /// <inheritdoc/>
        public event SendSequenceSeriesMetadataDelegate SendSequenceSeriesMetadata;
        /// <inheritdoc/>
        public event SendSequenceDelegate SendSequence;
        /// <inheritdoc/>
        public event SendDataToSourceDelegate SendDataToSource;
        /// <inheritdoc/>
        public event ParsingErrorDelegate ParsingError;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.Parser"/> class.
        /// </summary>
        protected Parser()
        {
        }

        /// <inheritdoc/>
        public abstract void Parse(byte[] data, int offset, int count);

        /// <inheritdoc/>
        public abstract void Reset();

        /// <summary>
        /// Notifies the send sequence series metadata.
        /// </summary>
        /// <param name="metadata">Metadata.</param>
        protected virtual void NotifySendSequenceSeriesMetadata(SequenceSeriesMetadata metadata)
        {
            if (SendSequenceSeriesMetadata != null) {
                SendSequenceSeriesMetadata(metadata);
            }
        }

        /// <summary>
        /// Notifies the send sequence.
        /// </summary>
        /// <param name="sequence">Sequence.</param>
        protected void NotifySendSequence(Sequence sequence)
        {
            if (SendSequence != null) {
                SendSequence(sequence);
            }
        }

        /// <summary>
        /// Notifies the send data to source.
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="count">Count.</param>
        protected void NotifySendDataToSource(byte[] data, int offset, int count)
        {
            if (SendDataToSource != null) {
                SendDataToSource(data, offset, count);
            }
        }

        /// <summary>
        /// Notifies the send data to source.
        /// </summary>
        /// <param name="data">Data.</param>
        protected void NotifySendDataToSource(byte[] data)
        {
            NotifySendDataToSource(data, 0, data.Length);
        }

        /// <summary>
        /// Notifies the parsing error.
        /// </summary>
        /// <param name="showInteractive">If set to <c>true</c> show interactive.</param>
        /// <param name="cause">Cause.</param>
        /// <param name="englishMessage">English message.</param>
        /// <param name="localizedMessage">Localized message.</param>
        protected void NotifyParsingError(bool showInteractive, Exception cause, string englishMessage, string localizedMessage)
        {
            if (cause != null) {
                Logging.Logger.Log(cause);
            }

            NotifyParsingError(showInteractive, englishMessage, localizedMessage);
        }

        /// <summary>
        /// Notifies the parsing error.
        /// </summary>
        /// <param name="showInteractive">If set to <c>true</c> show interactive.</param>
        /// <param name="englishMessage">English message.</param>
        /// <param name="localizedMessage">Localized message.</param>
        protected void NotifyParsingError(bool showInteractive, string englishMessage, string localizedMessage)
        {
            if (ParsingError != null) {
                ParsingError(
                    showInteractive,
                    null,
                    englishMessage,
                    localizedMessage);
            }
        }

        /// <summary>
        /// Notifies the parsing error.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="languageDomain">Language domain.</param>
        /// <param name="showInteractive">If set to <c>true</c> show interactive.</param>
        /// <param name="cause">Cause.</param>
        /// <param name="message">Message.</param>
        /// <param name="arguments">Arguments.</param>
#if MICRO_FRAMEWORK
        protected void NotifyParsingError(bool showInteractive, Exception cause, string message)
#else
        protected void NotifyParsingError(I18n i18n, string languageDomain, bool showInteractive, Exception cause, string message, params object[] arguments)
#endif
        {
#if MICRO_FRAMEWORK
            NotifyParsingError(showInteractive, cause, message, message);
#else
            TranslationString tr = i18n.TrObject(languageDomain, message, arguments);
            NotifyParsingError(showInteractive, cause, tr.UntranslatedString, tr.Tr);
#endif
        }

        /// <summary>
        /// Notifies the parsing error.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="languageDomain">Language domain.</param>
        /// <param name="showInteractive">If set to <c>true</c> show interactive.</param>
        /// <param name="message">Message.</param>
        /// <param name="arguments">Arguments.</param>
#if MICRO_FRAMEWORK
        protected void NotifyParsingError(bool showInteractive, string message)
#else
        protected void NotifyParsingError(I18n i18n, string languageDomain, bool showInteractive, string message, params object[] arguments)
#endif
        {
#if MICRO_FRAMEWORK
            NotifyParsingError(showInteractive, message, message);
#else
            TranslationString tr = i18n.TrObject(languageDomain, message, arguments);
            NotifyParsingError(showInteractive, tr.UntranslatedString, tr.Tr);
#endif
        }
    }
}

