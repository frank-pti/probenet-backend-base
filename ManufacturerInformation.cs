/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Manufacturer information.
    /// </summary>
    public class ManufacturerInformation
    {
        private string name;
        private byte[] icon;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.ManufacturerInformation"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="icon">Icon.</param>
        public ManufacturerInformation (string name, byte[] icon)
        {
            this.name = name;
            this.icon = icon;
            IsCatchAll = false;
            ContainsAll = false;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get {
                return name;
            }
        }

        /// <summary>
        /// Gets the icon (e.g. company sign).
        /// </summary>
        /// <value>The icon.</value>
        public byte[] Icon {
            get {
                return icon;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is a "catch all" instance.
        /// </summary>
        /// <value><c>true</c> if this instance is catch all; otherwise, <c>false</c>.</value>
        /// <remarks>A "catch all" instance can be used for catching all manufactorer information instances that do not
        /// match another manufactorer information name (e.g. therefore listed as "unknown")</remarks>
        public bool IsCatchAll {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this manufacturer information lists all (e.g. listed as "all").
        /// </summary>
        /// <value><c>true</c> if contains all; otherwise, <c>false</c>.</value>
        public bool ContainsAll {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is special.
        /// </summary>
        /// <value><c>true</c> if this instance is special; otherwise, <c>false</c>.</value>
        public bool IsSpecial {
            get {
                return IsCatchAll || ContainsAll;
            }
        }
    }
}

