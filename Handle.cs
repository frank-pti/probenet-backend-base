/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using ProbeNet.Messages.Raw;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Internationalization;
using ProbeNet.Messages.Interface;
#endif

namespace ProbeNet.Backend
{
    /// <summary>
    /// Base class for all handles
    /// </summary>
    public abstract class Handle : DeviceHandle
    {
        /// <summary>
        /// Delegate method for serial number changed event.
        /// </summary>
        protected delegate void SerialNumberChangedDelegate(string serialNumber);
        /// <summary>
        /// Occurs when serial number changed.
        /// </summary>
        protected event SerialNumberChangedDelegate SerialNumberChanged;

        public delegate void DeviceDescriptionChangedDelegate(
            ISequenceDescription sequenceDescription, IMeasurementDescription measurementDescription);
        public event DeviceDescriptionChangedDelegate DeviceDescriptionChanged;
        /// <summary>
        /// The i18n.
        /// </summary>
#if MICRO_FRAMEWORK
        private readonly IList installedLanguages;
        private IDictionary deviceDescriptions;
#else
        protected I18n i18n;
        private readonly IList<String> installedLanguages;
        private IDictionary<string, DeviceDescription> deviceDescriptions;
#endif
        private string serialNumber;


        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.Handle"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="installedLanguages">Installed languages.</param>
        /// <param name="serialNumber">Serial number.</param>
        /// <param name="deviceDescriptions">Device descriptions.</param>
#if MICRO_FRAMEWORK
        protected Handle(IList installedLanguages, string serialNumber, IDictionary deviceDescriptions)
        {
#else
        protected Handle(I18n i18n, IList<String> installedLanguages, string serialNumber, IDictionary<string, DeviceDescription> deviceDescriptions)
        {
            this.i18n = i18n;
#endif
            this.installedLanguages = installedLanguages;
            SerialNumber = serialNumber;
            this.deviceDescriptions = deviceDescriptions;
        }

        /// <summary>
        /// Gets the list of installed languages.
        /// </summary>
        /// <value>The installed languages.</value>
#if MICRO_FRAMEWORK
        protected IList InstalledLanguages
#else
        protected IList<string> InstalledLanguages
#endif
        {
            get
            {
                return this.installedLanguages;
            }
        }

#if !MICRO_FRAMEWORK
        /// <summary>
        /// Gets the translatable caption.
        /// </summary>
        /// <value>The caption.</value>
        public virtual TranslationString Caption
        {
            get
            {
                object[] attributes = GetType().GetCustomAttributes(typeof(TranslatableCaptionAttribute), false);
                if (attributes.Length == 0) {
                    throw new ArgumentException("The type must have a TranslatableCaption attribute");
                }

                TranslatableCaptionAttribute attribute = (TranslatableCaptionAttribute)(attributes[0]);
                return attribute.GetTranslation(i18n);
            }
        }

        /// <summary>
        /// Gets the translatable device information.
        /// </summary>
        /// <value>The device information.</value>
        public TranslationString DeviceInformation
        {
            get
            {
                object[] attributes = GetType().GetCustomAttributes(typeof(TranslatableDeviceInformationAttribute), false);
                if (attributes.Length == 0) {
                    return null;
                }

                TranslatableDeviceInformationAttribute attribute = (TranslatableDeviceInformationAttribute)(attributes[0]);
                return attribute.GetTranslation(i18n);
            }
        }
#endif

        /// <summary>
        /// Gets the icon.
        /// </summary>
        /// <value>The icon.</value>
        public byte[] Icon
        {
            get
            {
#if MICRO_FRAMEWORK
                object[] attributes = new object[0];
#else
                object[] attributes = GetType().GetCustomAttributes(typeof(ResourceIconAttribute), false);
#endif
                if (attributes.Length == 0) {
                    return null;
                }

                ResourceIconAttribute attribute = (ResourceIconAttribute)(attributes[0]);
                return attribute.GetIconFromAssembly(GetType().Assembly);
            }
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public string Identifier
        {
            get
            {
#if MICRO_FRAMEWORK
                object[] attributes = new object[0];
#else
                object[] attributes = GetType().GetCustomAttributes(typeof(SourceTypeIdentifierAttribute), false);
#endif
                if (attributes.Length == 0) {
                    return null;
                }

                SourceTypeIdentifierAttribute attribute = (SourceTypeIdentifierAttribute)(attributes[0]);
                return attribute.Identifier;
            }
        }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        /// <value>The serial number.</value>
        public string SerialNumber
        {
            get
            {
                return serialNumber;
            }
            set
            {
                if (serialNumber != value) {
                    serialNumber = value;
                }
                if (SerialNumberChanged != null) {
                    SerialNumberChanged(serialNumber);
                }
            }
        }

        /// <summary>
        /// Gets the device descriptions.
        /// </summary>
        /// <value>The device descriptions.</value>
#if MICRO_FRAMEWORK
        protected IDictionary DeviceDescription
#else
        protected IDictionary<string, DeviceDescription> DeviceDescriptions
#endif
        {
            get
            {
                return deviceDescriptions;
            }
        }

        /// <summary>
        /// Gets the amount of example data.
        /// </summary>
        /// <value>The example data count.</value>
        public abstract int ExampleDataCount
        {
            get;
        }

        /// <summary>
        /// Gets the example data with the specified index.
        /// </summary>
        /// <returns>The example data.</returns>
        /// <param name="index">Index.</param>
        public abstract byte[] GetExampleData(int index);

        /// <summary>
        /// Update the device description from device.
        /// </summary>
        /// <remarks>Default implementation is to do nothing if device does not support description updates.</remarks>
        public virtual void UpdateDeviceDescription()
        {
        }

        protected void NotifyDeviceDescriptionChanged(ISequenceDescription sequenceDescription, IMeasurementDescription measurementDescription)
        {
            if (DeviceDescriptionChanged != null) {
                DeviceDescriptionChanged(sequenceDescription, measurementDescription);
            }
        }
    }
}
