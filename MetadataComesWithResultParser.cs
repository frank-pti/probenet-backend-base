/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if MICRO_FRAMEWORK
using System.Collections;
using ProbeNet.Messages.Raw;
#else
using System.Collections.Generic;
using ProbeNet.Messages.Translatable;
#endif

namespace ProbeNet.Backend
{
    /// <summary>
    /// This is the base class of the parser of devices where the metadata is sent with the result in one datagram.
    /// </summary>
    public abstract class MetadataComesWithResultParser : Parser
    {
        private ProbeNet.Messages.Base.SequenceSeriesMetadata lastSentMetadata;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.MetadataComesWithResultParser"/> class.
        /// </summary>
        public MetadataComesWithResultParser()
        {
        }

        /// <inheritdoc/>
        public override void Reset()
        {
            lastSentMetadata = null;
        }

        /// <inheritdoc/>
        protected override void NotifySendSequenceSeriesMetadata(ProbeNet.Messages.Base.SequenceSeriesMetadata metadata)
        {
            if (lastSentMetadata != metadata) {
                base.NotifySendSequenceSeriesMetadata(metadata);
                lastSentMetadata = metadata;
            }
        }

        /// <summary>
        /// Builds the metadata.
        /// </summary>
        /// <returns>The metadata.</returns>
        /// <param name="sequenceId">Sequence identifier.</param>
        /// <param name="measurementId">Measurement identifier.</param>
        /// <param name="parameters">Parameters.</param>
        /// <param name="textualAttributes">Textual attributes.</param>
#if MICRO_FRAMEWORK
        protected virtual SequenceSeriesMetadata BuildMetadata(
            string sequenceId, string measurementId, IDictionary parameters, IDictionary textualAttributes)
#else
        protected virtual SequenceSeriesMetadata BuildMetadata(
            string sequenceId,
            string measurementId,
            IDictionary<string, object> parameters = null,
            IDictionary<string, string> textualAttributes = null)
#endif
        {
            if (textualAttributes == null) {
#if MICRO_FRAMEWORK
                textualAttributes = new Hashtable();
#else
                textualAttributes = new Dictionary<string, string>();
#endif
            }
#if MICRO_FRAMEWORK
            return new SequenceSeriesMetadata(sequenceId, Guid.NewGuid()) {
                Measurements = new Hashtable()
#else
            return new SequenceSeriesMetadata(sequenceId, Guid.NewGuid()) {
                Measurements = new Dictionary<string, MeasurementMetadata>()
#endif
                {
                    {
                        measurementId,
                        new MeasurementMetadata () {
                            Enabled = true,
                            Parameters = parameters
                        }
                    }
                },
                TextualAttributes = textualAttributes
            };

        }

        /// <summary>
        /// Builds the sequence.
        /// </summary>
        /// <returns>The sequence.</returns>
        /// <param name="designation">Designation.</param>
        /// <param name="results">Results.</param>
        /// <param name="alignment">Alignment.</param>
        /// <param name="curve">Curve.</param>
        protected Sequence BuildSequence(
            String designation,
#if MICRO_FRAMEWORK
 Hashtable results,
#else
 IDictionary<string, Nullable<double>> results,
#endif
 Alignment alignment = null,
#if MICRO_FRAMEWORK
 IList curve = null,
#else
 IList<double[]> curve = null,
#endif
 bool active = true)
        {
            return new Sequence() {
#if MICRO_FRAMEWORK
                Measurements = new Hashtable()
#else
                Measurements = new Dictionary<string, Measurement>()
#endif
                 {
                     {
                         designation,
                         new Measurement () {
                             Active = active,
                             Alignment = alignment,
                             Uuid = Guid.NewGuid(),
                             Results = results,
                             Curve = curve
                         }
                     }
                 }
            };
        }
    }
}

