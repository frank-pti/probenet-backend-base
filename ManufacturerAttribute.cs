/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Manufacturer attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ManufacturerAttribute : Attribute
    {
        private const string PropertyName = "Manufacturer";

        private ManufacturerInformation manufacturer;

#if MICRO_FRAMEWORK
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.ManufacturerAttribute"/> class.
        /// </summary>
        /// <param name="manufacturer">Manufacturer information.</param>
        public ManufacturerAttribute(ManufacturerInformation manufacturer)
        {
            this.manufacturer = manufacturer;
        }
#else
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.ManufacturerAttribute"/> class.
        /// </summary>
        /// <param name="type">Type.</param>
        public ManufacturerAttribute (Type type)
        {
            PropertyInfo manufacturerProperty = type.GetProperty(PropertyName);
            if (manufacturerProperty == null ||
                !IsCorrectType(manufacturerProperty.PropertyType, typeof(ManufacturerInformation))) {
                throw new ArgumentException(String.Format(
                    "Specified type does not contain a '{0}' property or it it is not type '{1}'", 
                    PropertyName, typeof(ManufacturerInformation)), "type");
            }
            if (!manufacturerProperty.CanRead) {
                throw new ArgumentException(
                    String.Format("Property '{0}' cannot be read", manufacturerProperty.Name), "type");
            }
            manufacturer = manufacturerProperty.GetGetMethod().Invoke(null, null) as ManufacturerInformation;
        }
#endif

        /// <summary>
        /// Gets the manufacturer.
        /// </summary>
        /// <value>The manufacturer.</value>
        public ManufacturerInformation Manufacturer
        {
            get
            {
                return manufacturer;
            }
        }

        private static bool IsCorrectType(Type propertyType, Type otherType)
        {
            return propertyType.Equals(otherType) || propertyType.IsSubclassOf(otherType);
        }
    }
}

